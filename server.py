#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Clase (y programa principal) para un servidor de eco en UDP simple
"""

import random
import socketserver
import sys
import simplertp

PORT = int(sys.argv[2])
audio_file = str(sys.argv[3])
ALEAT = random.randint(1, 20)  # Nos devuelve un número entero aleatorio comprendido entre 1 y 20, tal que a <= N <= b
IP = str(sys.argv[1])

if len(sys.argv) != 4:
    print("Usage: python3 server.py <IP> <port> <audio_file>")




class EchoHandler(socketserver.BaseRequestHandler):
    """
    UDP echo handler class
    """
    dicc = {}

    def handle(self):
        data1 = self.request[0]
        sock = self.request[1]
        data = data1.decode("utf-8")
        print(data)
        firstNumberIP= data.split()[1].split("@")[1].split()[0][0]
        Element1 = data.split()[1].split("@")[1].split()[0][3]
        Element2 = data.split()[1].split("@")[1].split()[0][7]
        Element3 = data.split()[1].split("@")[1].split()[0][10]
        Element4 = data.split()[1][0:4]
        method = data.split()[0]
        receptor = data.split()[1].split("sip:")[1]
        # userDIC = receptor.split("@")[0]
        # ipDIC = receptor.split("@")[1]
        # self.dicc["user"] = userDIC
        # self.dicc["ip"] = ipDIC
        # ipC = self.client_address[0]
        # portC = self.client_address[1]

        v = "v = 0"
        o = "o = " + str(receptor)
        t = "t = 0"
        m = "m = audio 67876 RTP"
        body = "\r\n" + "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesion" + "\r\n" + t + "\r\n" + m

        if method == "INVITE" or method == "BYE" or method == "ACK":
                if method == "INVITE":
                    respond = "SIP/2.0 200 OK" + body
                    sock.sendto(respond.encode('utf-8'), self.client_address)
                # elif method == "ACK":
                #     RTP_header = simplertp.RtpHeader()
                #     RTP_header.set_header(pad_flag=0, ext_flag=0, cc=0, ssrc=ALEAT)
                #     audio = simplertp.RtpPayloadMp3(audio_file)
                #     simplertp.send_rtp_packet(RTP_header, audio, ipC, portC)

                else:
                    if firstNumberIP != "1" or Element1 != "." or Element2 != "." or data.split()[2] != "SIP/2.0" or Element3 != "." or Element4 != "sip:":
                        respond = "SIP/2.0 400 Bad Request"
                        sock.sendto(respond.encode('utf-8'), self.client_address)

        else:
            respond = "SIP/2.0 405 Method Not Allowed"
            sock.sendto(respond.encode('utf-8'), self.client_address)


def main():
    try:
        serv = socketserver.UDPServer(("", PORT), EchoHandler)
        print("Listening...")
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizando")
        sys.exit(0)


if __name__ == "__main__":
    main()
