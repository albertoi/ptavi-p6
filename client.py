#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Programa cliente que abre un socket a un servidor
"""

import socket
import sys


def main():

        try:
            with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
                    SERVER = 'localhost'
                    PORT = int(sys.argv[2].split(":")[1])
                    receiver = sys.argv[2].split('@')[0]
                    ip = str(sys.argv[2].split('@')[1].split(":")[0])
                    method = str(sys.argv[1])   # método de petición SIP
                    SOLICITUD = method + " " + "sip:" + str(receiver) + "@" + str(ip) + " SIP/2.0" + '\r\n'
                    c = "Content-Type: application/sdp"
                    c1 = "Content-Length:"
                    v = "v = 0"
                    o = "o = " + receiver + "@" + " " + ip
                    t = "t = 0"
                    m = "m = audio 34543 RTP"
                    body = c + "\r\n" + c1 + "\r\n" + "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesion" + "\r\n" + t + "\r\n" + m
                    long = len(body)
                    body1 = c + "\r\n" + c1 + " " + str(long) + "\r\n" + "\r\n" + v + "\r\n" + o + "\r\n" + "s = mi sesion" + "\r\n" + t + "\r\n" + m

                    if method == "INVITE":
                        SOLICITUD += body1
                    my_socket.sendto(SOLICITUD.encode('utf-8'), (SERVER, PORT))
                    data = my_socket.recv(1024)
                    Answer = data.decode("utf-8")
                    answer = Answer.split("\r\n")[0]
                    if method == "INVITE" and answer == "SIP/2.0 200 OK":
                        data1 = "ACK " + " " + "sip:" + str(receiver) + "@" + str(ip) + " " + "SIP/2.0" + '\r\n'
                        my_socket.sendto(data1.encode('utf-8'), (SERVER, PORT))

                    print("Recibido:")
                    print(data.decode('utf-8'))

            print("Terminando programa...")
        except ConnectionRefusedError:
            print("Error ")


if __name__ == "__main__":
    metodo = ["INVITE", "ACK", "BYE"]
    method = sys.argv[1]
    if len(sys.argv) != 3:
        print("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
    elif method != metodo[0] and method != metodo[1] and method != metodo[2]:
        print("Usage: python3 client.py <method> <receiver>@<IP>:<SIPport>")
    main()
